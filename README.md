# U-Net with DWM loss for image segmentation 

**Research**
* <a href=https://arxiv.org/pdf/1505.04597.pdf>U-Net: Convolutional Networks for Biomedical
Image Segmentation</a>
* <a href=https://arxiv.org/pdf/1802.07465.pdf>MULTICLASS WEIGHTED LOSS FOR INSTANCE SEGMENTATION
OF CLUTTERED CELLS</a>

**Data**
* <a href=https://www.kaggle.com/c/data-science-bowl-2018>Kaggle  2018 Data Science Bowl</a>

**Work in progress**

The interface, postprocessing and packaging is still incomplete.

**DWM Loss Visualization**

![](img/dwmloss.png)

**Monitor Training with the Tensorboard**

![1](img/losstb.png) 
![2](img/predtb.png)
