from path import Path


PROJECT_ROOT_DIR = Path(__file__).parent

DATA_DIR = PROJECT_ROOT_DIR / 'data'
DATA_DIR.mkdir_p()

RAW_DATA_DIR = DATA_DIR / 'raw'
RAW_DATA_DIR.mkdir_p()

PROCESSED_DATA_DIR = DATA_DIR / 'processed'
PROCESSED_DATA_DIR.mkdir_p()

MODELS_DIR = PROJECT_ROOT_DIR / 'models'
MODELS_DIR.mkdir_p()

CHECKPOINTS_DIR = MODELS_DIR / 'checkpoints'  # cartella dove si vogiono salvare i checkpoints
CHECKPOINTS_DIR.mkdir_p()

SUMMARIES_DIR = MODELS_DIR / 'summaries'  # cartella dove salvare le summaries da accedere con la board
SUMMARIES_DIR.mkdir_p()

TRAIN_SUMMARIES_DIR = SUMMARIES_DIR / 'train'
TRAIN_SUMMARIES_DIR.mkdir_p()

KAGGLE_AUTH = PROJECT_ROOT_DIR / '.kaggle' / 'auth.json'