# Dataset Constant

N_CLASS = 3  # augmented
IMG_CHANNELS = 3
N_TRAIN_EXAMPLES = 670

# preprocessing constant

IN_SIZE = 268
OUT_SIZE = 180
EDGE = 44

# model hyperparameter

N_FILTERS = 64
N_UP_BLOCKS = 4
KERNEL_SIZE = 3
POOL_SIZE = 2