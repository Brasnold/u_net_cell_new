import tensorflow as tf
from tensorflow.data import Dataset, Iterator
from config import PROCESSED_DATA_DIR
from hyperparams import IN_SIZE, OUT_SIZE, EDGE, N_TRAIN_EXAMPLES
from skimage import io
import numpy as np


FLAGS = tf.flags.FLAGS

tf.flags.DEFINE_integer('preprocess_threads', 8,
                            """Parallel cpu threads for preprocessing""")



def input_pipeline(paths):
    img_path, mask_path, wmap_path = tf.unstack(paths)

    #input load and decoding
    img_file = tf.read_file(img_path)
    mask_file = tf.read_file(mask_path)

    img = tf.image.decode_png(img_file, channels=3)
    mask = tf.image.decode_png(mask_file, channels=1)
    wmap = tf.py_func(lambda path: io.imread(str(path)[2:-1]).astype(np.float32),    #tf wrapper per leggere .tiff
                      [wmap_path],
                      [tf.float32])


    #online preprocessing & data augmentation
    combined = tf.concat([tf.cast(img, tf.float32),
                          tf.cast(mask, tf.float32),
                          tf.reshape(wmap, tf.shape(mask))], axis=-1)
    combined_pad = tf.pad(combined, mode='SYMMETRIC', paddings=[[EDGE, EDGE], [EDGE, EDGE], [0, 0]])
    combined_crop = tf.random_crop(combined_pad, size=[IN_SIZE,IN_SIZE,5])

    img = combined_crop[:, :, :3]
    mask = tf.cast(combined_crop[EDGE: OUT_SIZE + EDGE, EDGE: OUT_SIZE + EDGE, 3], tf.int32)
    wmap = combined_crop[EDGE: OUT_SIZE + EDGE, EDGE: OUT_SIZE + EDGE, 4]

    img = 2*(img/255) - 1

    return img , mask, wmap


def get_train_batch(batch_size):
    #lista di tuple: (img_path, mask_path, wmap_path)
    paths = list(map(lambda path: (path / path.name + '.png',
                            path / path.name + '_mask.png',
                            path / path.name + '_weightsmap.tiff'),
                     PROCESSED_DATA_DIR.dirs()
            ))

    #dataset di paths
    tr_data = Dataset.from_tensor_slices(paths)

    tr_data = tr_data.shuffle(N_TRAIN_EXAMPLES).repeat()\
            .map(input_pipeline, num_parallel_calls=FLAGS.preprocess_threads).prefetch(100*batch_size).batch(batch_size)

    iterator = Iterator.from_structure((tf.float32, tf.int32, tf.float32),
            ((batch_size, IN_SIZE, IN_SIZE, 3), (batch_size, OUT_SIZE, OUT_SIZE), (batch_size, OUT_SIZE, OUT_SIZE))
    )

    iter_init = iterator.make_initializer(tr_data)

    imgs, masks, wmaps = iterator.get_next()
    tf.summary.image('image', tf.expand_dims(imgs[0, EDGE:EDGE + OUT_SIZE, EDGE:EDGE + OUT_SIZE, :], 0), collections=['period'])
    tf.summary.image('ground truth',
                     tf.reshape(tf.cast(masks[0,:,:], tf.float32), [1,OUT_SIZE, OUT_SIZE,1]), collections=['period'])
    tf.summary.image('wheights map',
                     tf.reshape(wmaps[0, :, :], [1, OUT_SIZE, OUT_SIZE, 1]),
                     collections=['period'])

    return imgs, masks, wmaps, iter_init