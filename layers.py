import tensorflow as tf


FLAGS = tf.flags.FLAGS

tf.flags.DEFINE_string('parameter_server', 'gpu',             # gpu se è disponibile una e una sola gpu
                        """Device to allocate variables.""")  # cpu se non è disponibile una gpu, o se lo è più di una gpu


WEIGHTS_INIT = tf.contrib.layers.xavier_initializer
BIAS_INIT = lambda: tf.constant_initializer(0.1)
ACTIVATION = tf.nn.selu


def variable(name, shape, initializer):
    with tf.device('/{}:0'.format(FLAGS.parameter_server)):
        var = tf.get_variable(name, shape, initializer=initializer, dtype=tf.float32)

    return var


def variable_weights(shape):
    var = variable('weights', shape, initializer=WEIGHTS_INIT())

    return var


def variable_bias(shape):
    var = variable('bias', shape, initializer=BIAS_INIT())

    return var


def conv(scope, x, n_filters, kernel_size, stride=1, activation=ACTIVATION, padding='VALID'):
    with tf.variable_scope(scope):
        w = variable_weights([kernel_size, kernel_size, x.shape[3], n_filters])
        b = variable_bias([n_filters])

    x = tf.nn.conv2d(x, w, strides=[1, stride, stride, 1], padding=padding)

    return activation(x + b) if activation is not None else x + b


def deconv(scope, x, n_filters, factor, activation=ACTIVATION):
    with tf.variable_scope(scope):
        w = variable_weights([factor, factor, n_filters, x.shape[3]])
        b = variable_bias([n_filters])

    output_shape = tf.stack([x.shape[0], x.shape[1] * 2, x.shape[2] * 2, n_filters])

    x = tf.nn.conv2d_transpose(x, w, output_shape, strides=[1, factor, factor, 1], padding='SAME')

    return activation(x + b) if activation is not None else x + b


def max_pool(x, k, padding='SAME'):
    return tf.nn.max_pool(x, ksize=[1, k, k, 1], strides=[1, k, k, 1], padding=padding)
