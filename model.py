import tensorflow as tf
from layers import conv, deconv, max_pool
from tf_utils import crop_and_concat


def conv_block(scope, x, n_filters, kernel_size, pool_size):
    with tf.variable_scope(scope):
        x = conv('conv1', x, n_filters, kernel_size)
        x = h = conv('conv2', x, n_filters, kernel_size)

        x = max_pool(x, pool_size) if pool_size is not None else None  # il blocco centrale non usa max-pool

    return x, h


def deconv_block(scope, inputs, n_filters, kernel_size, pool_size, crop=True):
    with tf.variable_scope(scope):
        x, h = inputs

        x = deconv('deconv', x, n_filters, pool_size)

        x = crop_and_concat(h, x) if crop else tf.concat([h, x], axis=3)  # defoult VALID pad implica croppare

        x = conv('conv1', x, n_filters, kernel_size)
        x = conv('conv2', x, n_filters, kernel_size)

    return x


def model_graph(input, n_class, n_dwn_blocks, features, kernel_size, pool_size):
    x = input
    layers = []

    # down-conv
    for i in range(n_dwn_blocks - 1):
        x, h = conv_block('dwn_blck_{}'.format(i), x, features, kernel_size, pool_size)
        layers.append(h)
        features *= 2

    # blocco centrale
    _, x = conv_block('cntrl_blck', x, features, kernel_size, None)
    features //= 2

    # up-conv
    for i in range(n_dwn_blocks - 2, -1, -1):
        x = deconv_block('up_blck_{}'.format(i), [x, layers[i]], features, kernel_size, pool_size)
        features //= 2

    # logits mask
    logits = conv('outputmap', x, n_class, 1, activation=None)

    return logits


def compute_cross_entropy(logits, masks, pw=None):
    cross_entropy = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=logits, labels=masks)
    # in caso di pesi: somma sui pixel, media sul batch
    cross_entropy = \
        tf.reduce_mean(tf.reduce_sum(pw * cross_entropy, [1, 2])) if pw != None else tf.reduce_mean(cross_entropy)
    tf.summary.scalar('cross_entropy', cross_entropy)

    return cross_entropy


def compute_weights_loss(wd):
    weights_loss = [tf.reduce_sum(w ** 2) for w in tf.global_variables() if 'weights' in w.name]
    weights_loss = tf.add_n(weights_loss) * wd
    tf.summary.scalar('weights_loss', weights_loss)

    return weights_loss


def compute_predictions(logits):
    predictions = tf.argmax(logits, 3)

    out_size = logits.shape[1]  # supponendo output quadrato
    tf.summary.image('prediction',
                     tf.reshape(tf.cast(predictions[0, :, :], tf.float32), [1, out_size, out_size, 1]),
                     collections=['period'])

    return predictions