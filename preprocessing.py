##################################################################################
#
#   Modulo che effettua un trattamento preliminare dei dati
#   che non può essere facilmente effettuato real-time durante il training.
#
#   Pipeline implementata:
#
#   -Crea l'augmented mask aggregando le maschere delle singole istanze
#    e aggiungendo la classe touching pixel.
#   -Crea la weights map necessaria per implementare la DWM loss function.
#   -Salva l'immagine originale, l'augmented map e la weights map.
#
#
#
##################################################################################




from config import RAW_DATA_DIR, PROCESSED_DATA_DIR
from skimage import io, morphology
import numpy as np
from scipy import ndimage
import warnings
from tqdm import tqdm
from shutil import copyfile



BETA = 30      #i pesi dei pixels di background decadono in modo proporsionale a beta:
               #con beta piccolo si da importanza solo a una piccola area intorno alle cellule
               #con beta grande si da importanza a un'area più vasta di contorno


warnings.filterwarnings('ignore', category=UserWarning, module='skimage')   #ignora lo spam di warnings di skimage


############ Wrapper di skimage e ndimage per lavorare con la lista di maschere #####################################

def erosion(masks):
    return list(map(lambda x: morphology.binary_erosion(x, selem=morphology.square(3)),masks))

def dilation(masks):
    return list(map(lambda x: morphology.binary_dilation(x, selem=morphology.square(3)), masks))

def logical_xor(list1, list2):
    return list(map(lambda x: np.logical_xor(x[0], x[1]), zip(list1, list2)))

def find_contours(masks):
    return logical_xor(masks, erosion(masks))

def fill_holes(masks):
    return list(map(lambda x: ndimage.binary_fill_holes(x), masks))

def logical_and(images):
    return np.max(
            np.concatenate(list(map(lambda x: np.expand_dims(x, axis=-1), images)), axis=-1).astype(np.int32),
            axis=-1
        )

################################################################################################

##################### implementazione augmented mask e DWM ######################################

def touching_pixels(masks):
    boundary = logical_and(logical_xor(masks, dilation(masks)))
    complete = logical_and(masks)

    touching = boundary*complete

    return morphology.erosion(
            morphology.dilation(touching, selem=morphology.square(3)),
            selem=morphology.square(3)
        )

def augmented_mask(masks):

    complete = logical_and(fill_holes(masks))
    touching = touching_pixels(masks)

    return np.where(touching, 2, complete).astype(np.int32)


def class_weights(complete):

    count = [np.sum(complete==i) for i in range(3)]

    return [1/x if x!=0 else 0 for x in count]

def distance_weight_map(complete):
    inverse = np.logical_not(complete.astype(np.bool))

    base_w = class_weights(complete)

    dist = ndimage.morphology.distance_transform_edt(inverse)/float(BETA)

    dwm = 1 - np.minimum(dist, 1)

    for i in range(3):
        dwm = np.where(complete==i, dwm*base_w[i], dwm)

    return dwm

#######################################################################################################


def preprocessing_dataset():

    """lista di tuple: (id, img_path, masks_path)"""
    train_paths = list(map(lambda path: (path.name,                       #id
                                         path/'images'/path.name + '.png',
                                         path/'masks'/'*.png'),
                           RAW_DATA_DIR.dirs()))

    for n, elem in tqdm(enumerate(train_paths), total=len(train_paths)):  # printa barra di caricamento

        id, img_path, masks_path = elem

        masks = io.imread_collection(masks_path)
        aug = augmented_mask(masks)
        wmap = distance_weight_map(aug)

        dst_dir = PROCESSED_DATA_DIR / id
        dst_dir.mkdir_p()

        img_dst_path = dst_dir / id +'.png'
        mask_dst_path = dst_dir / id +'_mask.png'
        wm_dst_path = dst_dir / id + '_weightsmap.tiff'

        copyfile(img_path, img_dst_path)     #copio immagine nella train dir
        io.imsave(mask_dst_path, aug)
        io.imsave(wm_dst_path, wmap)


if __name__ == '__main__':
    preprocessing_dataset()
