import tensorflow as tf

def crop_and_concat(x1,x2):
    x1_shape = x1.get_shape().as_list()
    x2_shape = x2.get_shape().as_list()

    # offsets for the top left corner of the crop
    offsets = [0, (x1_shape[1] - x2_shape[1]) // 2, (x1_shape[2] - x2_shape[2]) // 2, 0]
    size = [-1, x2_shape[1], x2_shape[2], -1]

    x1_crop = tf.slice(x1, offsets, size)

    return tf.concat([x1_crop, x2], 3)