import tensorflow as tf
from model import model_graph, compute_cross_entropy, compute_weights_loss, compute_predictions
from input_train import get_train_batch
from config import CHECKPOINTS_DIR, TRAIN_SUMMARIES_DIR
from hyperparams import N_CLASS, N_FILTERS, N_UP_BLOCKS, KERNEL_SIZE, POOL_SIZE
import os
import time


FLAGS = tf.flags.FLAGS


tf.flags.DEFINE_integer('max_steps', 6700,
                            """Number of batches to run""")
tf.flags.DEFINE_integer('batch_size', 16,
                            """Batch size""")
tf.flags.DEFINE_integer('seed', 53,
                            """Random seed""")


tf.flags.DEFINE_float('weights_decay', 0.00005,
                          """Weights decay""")
tf.flags.DEFINE_float('learning_rate', 0.001,
                          """Learning rate""")


tf.flags.DEFINE_bool('restore_last', False,
                           """Restore last checkpoint""")
tf.flags.DEFINE_integer('restore', 0,
                           """Restore a specific checkpoint""")
tf.flags.DEFINE_integer('checkpoint_frequency', 670,
                            """When save a checkpoint""")
tf.flags.DEFINE_integer('summaries_frequency', 335,
                            """Heavy summaries frequency""")
tf.flags.DEFINE_boolean('log_device_placement', False,
                            """Whether to log device placement""")



tf.random_seed = FLAGS.seed


def train():

    ckp_dir = CHECKPOINTS_DIR
    train_summ_dir = TRAIN_SUMMARIES_DIR

    with tf.Graph().as_default():

        global_step = tf.get_variable('global_step', dtype=tf.int32, initializer=0, trainable=False)

        with tf.device('/cpu:0'):                                           #preprocessing in parallelo sulla cpu
            images, masks, wmaps, iter_init = get_train_batch(FLAGS.batch_size)

        logits = model_graph(images, N_CLASS, N_UP_BLOCKS, N_FILTERS, KERNEL_SIZE, POOL_SIZE)

        compute_predictions(logits)      #solo per creare la summary

        cross_entropy = compute_cross_entropy(logits, masks, wmaps)
        weights_loss = compute_weights_loss(FLAGS.weights_decay)

        loss = cross_entropy + weights_loss
        tf.summary.scalar('loss', loss)

        learning_rate = FLAGS.learning_rate
        tf.summary.scalar('learning_rate', learning_rate)

        optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)

        with tf.control_dependencies([optimizer.minimize(loss)]):
            train_op = tf.assign_add(global_step, 1)

        merged_base_summaries = tf.summary.merge_all()                           #summaries leggere da prendere in ogni step
        merged_all = tf.summary.merge([merged_base_summaries, tf.summary.merge_all(key='period')])

        init = tf.group(tf.global_variables_initializer(), iter_init)

        saver = tf.train.Saver()
        writer = tf.summary.FileWriter(train_summ_dir)

        log_config = tf.ConfigProto(log_device_placement=True)  if FLAGS.log_device_placement else None

        with tf.Session(config=log_config) as sess:

            sess.run(init)

            if FLAGS.restore != 0:
                saver.restore(sess, os.path.join(ckp_dir, 'checkpoint-{}'.format(FLAGS.restore)))
                print('Model restored at global-step {}\n'.format(tf.train.global_step(sess, global_step)))

            elif FLAGS.restore_last:
                saver.restore(sess, tf.train.latest_checkpoint(ckp_dir))
                print('Model restored at global-step {}\n'.format(tf.train.global_step(sess, global_step)))

            start_time = time.time()                # cronometiamo per valutare le performance del modello

            for i in range(1, FLAGS.max_steps + 1):
                merged = merged_all if i%FLAGS.summaries_frequency == 0 else merged_base_summaries

                _global_step, _cross_entropy, _summary = sess.run([train_op, cross_entropy, merged])
                writer.add_summary(_summary, _global_step)

                print('local step: {} global step: {} loss: {}'.format(i, _global_step, _cross_entropy))


                if i%FLAGS.checkpoint_frequency == 0 or i == FLAGS.max_steps:
                    saver.save(sess, os.path.join(ckp_dir, 'checkpoint'), global_step=global_step)
                    print('\nModel saved at global-step {}\n'.format(tf.train.global_step(sess, global_step)))

            current_time = time.time()
            duration = float(current_time - start_time)

            examples_per_sec = FLAGS.max_steps * FLAGS.batch_size/ duration
            sec_per_batch = duration / FLAGS.max_steps

            print('\nexamples per seconds: {}  seconds per batch: {}\n'.format(examples_per_sec, sec_per_batch))


def main(argv=None):
    train()

if __name__ == '__main__':
  tf.app.run()

