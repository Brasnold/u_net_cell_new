from skimage import io
from config import PROCESSED_DATA_DIR
import matplotlib.pyplot as plt
import warnings


warnings.filterwarnings('ignore', category=UserWarning, module='skimage')
warnings.filterwarnings('ignore', category=FutureWarning, module='skimage')


def visualize_train_examples(ids):
    for id in ids:
        print('\nVisualizing: {}\n'.format(id))

        img_path = PROCESSED_DATA_DIR / id / id + '.png'
        mask_path = PROCESSED_DATA_DIR / id / id  + '_mask.png'
        wmap_path = PROCESSED_DATA_DIR / id / id + '_weightsmap.tiff'

        image = io.imread(img_path)
        mask = io.imread(mask_path)
        wmap = io.imread(wmap_path)

        fig = plt.figure(figsize=(16, 8))

        fig.add_subplot(1,3,1)
        io.imshow(image)

        fig.add_subplot(1,3,2)
        io.imshow(mask)

        fig.add_subplot(1,3,3)
        io.imshow(wmap)

        plt.show()


if __name__ == '__main__':

    #id = "0a7d30b252359a10fd298b638b90cb9ada3acced4e0c0e5a3692013f432ee4e9"

    #id = '1a11552569160f0b1ea10bedbd628ce6c14f29edec5092034c2309c556df833e'

    id = '1b6044e4858a9b7cee9b0028d8e54fbc8fb72e6c4424ab5b9f3859bfc72b33c5'

    visualize_train_examples([id])





